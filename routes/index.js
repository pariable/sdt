var express = require('express');
var router = express.Router();
const util = require('../utility/helper');
const users = require('../services/users');
let redis = require('redis');

(async () => {
  client = redis.createClient(6379);

  client.on("error", (error) => console.error(`Error : ${error}`));

  await client.connect();
})();


router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

//task No 1
router.post('/newAccount', async function(req, res, next) {
    const u=req.body.userName;
    const p = req.body.userPwd;
    
    try {
      let datas=await users.getAccount(u,p,req.query.page);
      if(datas.data.length > 0){
        res.status(500).json({"code": 500, "msg": "account already exists"});
      }else{
        await users.setAccount(u,p)
        res.status(200).json({"code": 200, "msg": "OK"});
      }
      
    } catch (err) {
      console.error(`Error while getting datas `, err.message);
      next(err);
    }

    
});

//task No 2
router.post('/login', async function(req, res, next) {
    const u=req.body.userName;
    const p = req.body.userPwd;
    try{
      let datas=await users.getAccount(u,p);
      if(datas.data.length > 0){
        const token=util.randomAsciiString(8);

        //save to mysql db for token
        await users.updateAccount(u,p,token);

        //save to redis with 5 mins timelimit
        await client.set(token,JSON.stringify({"userName":u,score:0}),{
          EX: 30000, //wil be expire in 5 minutes
          NX: true,
        });

        res.status(200).json({"code": 200, "token": token});
      }else{
        res.status(500).json({"code": 500, "msg": "login failed!"});
      }
      
    }catch(err){
      res.status(500).json({"code": 500, "msg": err.message});
    }

    
});

//task No 3
router.post('/saveScore', async function(req, res, next) { 
  const token=req.body.token;
  const score = Number.parseFloat(req.body.myScore);
  let results;
  let isCached = false;

  try{

      if(Number.isNaN(score)){
        res.status(500).json({"code": 500, "msg": "bad score"});
        return;
      }

      let user=await users.getAccountByToken(token);
      
      const cacheResults = await client.get(token);
      if (cacheResults) {
        isCached = true;
        results = JSON.parse(cacheResults);
        
        if(parseFloat(results.score) < score){
          
          await client.del(token);

          await client.set(token,JSON.stringify({"userName":results.userName,"score":score}),{
            EX: 30000, //wil be expire in 5 minutes
            NX: true,
          });

        }

        res.status(200).json({"code": 200, "msg": "OK"});

      } else {
        res.status(500).json({"code": 500, "msg": "bad token"});
      }

  }catch(err){
    res.status(500).json({"code": 500, "msg": err.message});
  }
});

//task No 4
router.post('/leaderBoard', async function(req, res, next) { 
    const token = req.body.token;
    const cacheResults = await client.get(token);
      if (cacheResults) {
        results = JSON.parse(cacheResults);
        let lb=[];
        lb.push({"mine" : { "myRank": 1, "userName":results.userName, "score": results.score, }});
       
        res.status(200).json({"code": 200, data:lb});

      } else {
        res.status(500).json({"code": 500, "msg": "bad token"});
      }
})


module.exports = router;
