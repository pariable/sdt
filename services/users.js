const db = require('./db');
const helper = require('../utility/helper');
const config = require('../config/config');

async function getAccount(user="",pwd="",page = 1){
  const offset = helper.getOffset(page, config.listPerPage);
  const rows = await db.query(
    `SELECT * from users where userName='${user}' AND userPwd='${pwd}' LIMIT ${offset},${config.listPerPage}`
  );
  const data = helper.emptyOrRows(rows);
  const meta = {page};

  return {
    data,
    meta
  }
}

async function getAccountByToken(token=1){
  const rows = await db.query(
    `SELECT * from users where token='${token}' `
  );
  const data = helper.emptyOrRows(rows);
  
  return {
    data
  }
}

async function setAccount(user="",pwd=""){
  const rows = await db.query(
    `INSERT INTO  users (userName,userPwd) values('${user}','${pwd}') `
  );
  
  return "ok";
}

async function updateAccount(user="",pwd="",tkn=""){
  const rows = await db.query(
    `UPDATE  users set token='${tkn}' WHERE userName='${user}' and userPwd='${pwd}' `
  );
  
  return "ok";
}

module.exports = {
    getAccount,setAccount,updateAccount,getAccountByToken
}